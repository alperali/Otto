# Optical Character Recognition of Arabic Script
Academic work in partial fulfillment for the degree of Master of Science in Computer Engineering.

Refer to https://www.sciencedirect.com/science/article/pii/S0165168497001175 for details.

This was developed and built during 1992-94 using Turbo C++ for DOS.